Starview

Space exploration game written in Python. Uses procedural generation.

Objectives:

- Explore galaxy
- Scan planets with ship sensors
- Mine remotely sensed minerals
- Build ship sensor improvements using minerals
- Mine more minerals