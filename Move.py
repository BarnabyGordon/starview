from Tkinter import *
import Settings as s
import math as m
import random as r

def Highlight():
	s.scanarea = []
	s.coords = []
	s.scanx = []
	s.scany = []
	for a in range(s.planets):
		s.coords.append([s.x[a],s.y[a]])
	
	for a in s.coords:
		if a[0] > (s.ship_x+s.ship_offsetx)-s.scanrange and a[0] < (s.ship_x+s.ship_offsetx)+s.scanrange:
			if a[1] > (s.ship_y+s.ship_offsety)-s.scanrange and a[1] < (s.ship_y+s.ship_offsety)+s.scanrange:
				s.scanx.append(a[0])
				s.scany.append(a[1])

def PlanetColour(n):
	x = s.planetcolour[n]
	if x >= 0 and x < 10:
		return 'blue'
	if x >= 10 and x < 17:
		return 'green'
	if x >= 17 and x < 40:
		return 'red'
	if x >= 40 and x < 55:
		return 'purple'
	if x >= 55 and x < 70:
		return 'yellow'
	if x >= 70 and x < 100:
		return 'gray'

blueplanet1 = PhotoImage(file = 'Graphics/blueplanet1.gif')
blueplanet2 = PhotoImage(file = 'Graphics/blueplanet2.gif')
blueplanet3 = PhotoImage(file = 'Graphics/blueplanet3.gif')
blueplanet4 = PhotoImage(file = 'Graphics/blueplanet4.gif')
blueplanet5 = PhotoImage(file = 'Graphics/blueplanet5.gif')
blueplanet6 = PhotoImage(file = 'Graphics/blueplanet6.gif')
redplanet1 = PhotoImage(file = 'Graphics/redplanet1.gif')
redplanet2 = PhotoImage(file = 'Graphics/redplanet2.gif')
redplanet3 = PhotoImage(file = 'Graphics/redplanet3.gif')
redplanet4 = PhotoImage(file = 'Graphics/redplanet4.gif')
redplanet5 = PhotoImage(file = 'Graphics/redplanet5.gif')
redplanet6 = PhotoImage(file = 'Graphics/redplanet6.gif')
greenplanet1 = PhotoImage(file = 'Graphics/greenplanet1.gif')
greenplanet2 = PhotoImage(file = 'Graphics/greenplanet2.gif')
greenplanet3 = PhotoImage(file = 'Graphics/greenplanet3.gif')
greenplanet4 = PhotoImage(file = 'Graphics/greenplanet4.gif')
greenplanet5 = PhotoImage(file = 'Graphics/greenplanet5.gif')
greenplanet6 = PhotoImage(file = 'Graphics/greenplanet6.gif')


def Planet(n):
	x = s.planetcolour[n]
	r.seed(x)
	planet_size = r.randrange(s.planet_minsize,s.planet_maxsize)
	if x >= 0 and x < 10:
		if planet_size == 0:
			return blueplanet1
		if planet_size == 1:
			return blueplanet2
		if planet_size == 2:
			return blueplanet3
		if planet_size == 3:
			return blueplanet4
		if planet_size == 4:
			return blueplanet5
		if planet_size == 5:
			return blueplanet6
	if x >= 10 and x < 17:
		if planet_size == 0:
			return greenplanet1
		if planet_size == 1:
			return greenplanet2
		if planet_size == 2:
			return greenplanet3
		if planet_size == 3:
			return greenplanet4
		if planet_size == 4:
			return greenplanet5
		if planet_size == 5:
			return greenplanet6
	if x >= 17 and x < 40:
		if planet_size == 0:
			return redplanet1
		if planet_size == 1:
			return redplanet2
		if planet_size == 2:
			return redplanet3
		if planet_size == 3:
			return redplanet4
		if planet_size == 4:
			return redplanet5
		if planet_size == 5:
			return redplanet6
	if x >= 40 and x < 55:
		if planet_size == 0:
			return blueplanet1
		if planet_size == 1:
			return blueplanet1
		if planet_size == 2:
			return blueplanet1
		if planet_size == 3:
			return blueplanet1
		if planet_size == 4:
			return blueplanet1
		if planet_size == 5:
			return blueplanet1
	if x >= 55 and x < 70:
		if planet_size == 0:
			return blueplanet1
		if planet_size == 1:
			return blueplanet1
		if planet_size == 2:
			return blueplanet1
		if planet_size == 3:
			return blueplanet1
		if planet_size == 4:
			return blueplanet1
		if planet_size == 5:
			return blueplanet1
	if x >= 70 and x < 100:
		if planet_size == 0:
			return blueplanet1
		if planet_size == 1:
			return blueplanet1
		if planet_size == 2:
			return blueplanet1
		if planet_size == 3:
			return blueplanet1
		if planet_size == 4:
			return blueplanet1
		if planet_size == 5:
			return blueplanet1

def Planetsize():
	for y in s.y:
		r.seed(y)
		s.planetsize.append(r.randrange(s.planet_minsize,s.planet_maxsize))

def Planetcolour():
	for x in s.x:
		r.seed(x)
		s.planetcolour.append(r.randrange(0,100))

def Sat1():
	if (s.inventory['Feldspar'] - 20) < 0:
		print "Need More Resources!"
	else:
		s.satellite['Sat 0'] = s.satellite['Sat 0'] + 1
		s.inventory['Feldspar'] = s.inventory['Feldspar'] - 20

def Sat2():
	if (s.inventory['Feldspar'] - 50) < 0:
		print "Need More Resources!"
	else:
		s.satellite['Sat 1'] = s.satellite['Sat 1'] + 1
		s.inventory['Feldspar'] = s.inventory['Feldspar'] - 50
def Sat3():
	if (s.inventory['Feldspar'] - 80) < 0 or\
		(s.inventory['Magnetite'] - 30) < 0:
		print "Need More Resources!"
	else:
		s.satellite['Sat 2'] = s.satellite['Sat 2'] + 1
		s.inventory['Feldspar'] = s.inventory['Feldspar'] - 80
		s.inventory['Magnetite'] = s.inventory['Magnetite'] - 30
def Sat4():
	if (s.inventory['Feldspar'] - 100) < 0 or\
		(s.inventory['Magnetite'] - 50) < 0 or\
		(s.inventory['Titanium'] - 10) < 0:
		print "Need More Resources!"
	else:
		s.satellite['Sat 3'] = s.satellite['Sat 3'] + 1
		s.inventory['Feldspar'] = s.inventory['Feldspar'] - 100
		s.inventory['Magnetite'] = s.inventory['Magnetite'] - 50
		s.inventory['Titanium'] = s.inventory['Titanium'] - 10
def Sat5():
	if (s.inventory['Magnetite'] - 80) < 0 or\
		(s.inventory['Uranium'] - 30) < 0:
		print "Need More Resources!"
	else:
		s.satellite['Sat 4'] = s.satellite['Sat 4'] + 1
		s.inventory['Uranium'] = s.inventory['Uranium'] - 30
		s.inventory['Magnetite'] = s.inventory['Magnetite'] - 80
def Sat6():
	if (s.inventory['Uranium'] - 50) < 0 or\
		(s.inventory['Titanium'] -50) < 0:
		print "Need More Resources!"
	else:
		s.satellite['Sat 5'] = s.satellite['Sat 5'] + 1
		s.inventory['Uranium'] = s.inventory['Uranium'] - 50
		s.inventory['Titanium'] = s.inventory['Titanium'] - 50



		


