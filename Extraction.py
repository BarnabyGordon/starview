import Settings as s
import random as r

def Extract_Resource():
	if (s.inventory['Feldspar'] + s.shipextraction) <= s.storage: 
		if (s.amount["%sfeld"%(s.scanx[0]+s.ship_x)] - s.shipextraction) < 0 and s.scanlevel >= 0:
			 s.amount["%sfeld"%(s.scanx[0]+s.ship_x)] = 0
			 s.inventory['Feldspar'] = s.inventory['Feldspar'] + (s.amount["%sfeld"%(s.scanx[0]+s.ship_x)])*2
		elif s.scanlevel >= 0:
			s.inventory['Feldspar'] = s.inventory['Feldspar'] + s.shipextraction*2
			s.amount["%sfeld"%(s.scanx[0]+s.ship_x)] = s.amount["%sfeld"%(s.scanx[0]+s.ship_x)] - s.shipextraction

	if (s.inventory['Hydrogen'] + s.shipextraction) <= s.storage:
		if (s.amount["%shyd"%(s.scanx[0]+s.ship_x)] - s.shipextraction) < 0 and s.scanlevel >= 4:
			 s.amount["%shyd"%(s.scanx[0]+s.ship_x)] = 0
			 s.inventory['Hydrogen'] = s.inventory['Hydrogen'] + (s.amount["%shyd"%(s.scanx[0]+s.ship_x)])*2
		elif s.scanlevel >= 4:
			s.inventory['Hydrogen'] = s.inventory['Hydrogen'] + s.shipextraction*2
			s.amount["%shyd"%(s.scanx[0]+s.ship_x)] = s.amount["%shyd"%(s.scanx[0]+s.ship_x)] - s.shipextraction

	if (s.inventory['Uranium'] + s.shipextraction) <= s.storage:
		if (s.amount["%suran"%(s.scanx[0]+s.ship_x)] - s.shipextraction) < 0 and s.scanlevel >= 2:
			 s.amount["%suran"%(s.scanx[0]+s.ship_x)] = 0
			 s.inventory['Uranium'] = s.inventory['Uranium'] + (s.amount["%suran"%(s.scanx[0]+s.ship_x)])*2
		elif s.scanlevel >= 2:
			s.inventory['Uranium'] = s.inventory['Uranium'] + s.shipextraction*2
			s.amount["%suran"%(s.scanx[0]+s.ship_x)] = s.amount["%suran"%(s.scanx[0]+s.ship_x)] - s.shipextraction

	if (s.inventory['Oxygen'] + s.shipextraction) <= s.storage:
		if (s.amount["%soxy"%(s.scanx[0]+s.ship_x)] - s.shipextraction) < 0 and s.scanlevel >= 3:
			 s.amount["%soxy"%(s.scanx[0]+s.ship_x)] = 0
			 s.inventory['Oxygen'] = s.inventory['Oxygen'] + (s.amount["%soxy"%(s.scanx[0]+s.ship_x)])*2
		elif s.scanlevel >= 3:
			s.inventory['Oxygen'] = s.inventory['Oxygen'] + s.shipextraction*2
			s.amount["%soxy"%(s.scanx[0]+s.ship_x)] = s.amount["%soxy"%(s.scanx[0]+s.ship_x)] - s.shipextraction

	if (s.inventory['Magnetite'] + s.shipextraction) <= s.storage:
		if (s.amount["%smag"%(s.scanx[0]+s.ship_x)] - s.shipextraction) < 0 and s.scanlevel >= 1:
			 s.amount["%smag"%(s.scanx[0]+s.ship_x)] = 0
			 s.inventory['Magnetite'] = s.inventory['Magnetite'] + (s.amount["%smag"%(s.scanx[0]+s.ship_x)])*2
		elif s.scanlevel >= 1:
			s.inventory['Magnetite'] = s.inventory['Magnetite'] + s.shipextraction*2
			s.amount["%smag"%(s.scanx[0]+s.ship_x)] = s.amount["%smag"%(s.scanx[0]+s.ship_x)] - s.shipextraction

	if (s.inventory['Titanium'] + s.shipextraction) <= s.storage:
		if (s.amount["%stita"%(s.scanx[0]+s.ship_x)] - s.shipextraction) < 0 and s.scanlevel >= 5:
			 s.amount["%stita"%(s.scanx[0]+s.ship_x)] = 0
			 s.inventory['Titanium'] = s.inventory['Titanium'] + (s.amount["%stita"%(s.scanx[0]+s.ship_x)])*2
		elif s.scanlevel >= 5:
			s.inventory['Titanium'] = s.inventory['Titanium'] + s.shipextraction*2
			s.amount["%stita"%(s.scanx[0]+s.ship_x)] = s.amount["%stita"%(s.scanx[0]+s.ship_x)] - s.shipextraction



