from Tkinter import *
import random
import Settings as s
import Extraction as e

import Names as name
import math as a
import Scan as scan

root = Tk()
root.title("Starview")
root.update()
import Move as m

titlefont = ('CF Spaceship', '25')
mainfont = ('Zekton Free', '15')

Ship = PhotoImage(file = 'Graphics/Ship.gif')
Scan_Menu = PhotoImage(file = 'Graphics/Scan_Menu.gif')

def Build():
	Seed=[]
	s.x=[]
	s.y=[]
	s.planetsize = []
	mtext = ment.get()
	for i in mtext:
		Seed+=[ord(i)]

	random.seed(sum(Seed))

	for x in range(s.planets):
		s.x.append(int(random.gauss(0,5000)))
 
	for y in range(s.planets):
		s.y.append(int(random.gauss(0,5000)))

	m.Planetsize()
	m.Planetcolour()
	Refresh()

ment = StringVar()

w = Canvas(root,width=1024,height=768,bg='#%02x%02x%02x'%(3,22,36))
w.grid(column=0,row=0)

w.create_text(500,350,text="Enter Galaxy Name:",fill='#%02x%02x%02x'%(243,2,8),font=mainfont)

entrybox = Entry(w, textvariable=ment)
w.create_window(500,380,window=entrybox)

buildbutton = w.create_text(650,380,text='Make it so',fill='#%02x%02x%02x'%(243,2,8),font=mainfont,activefill='yellow')
w.tag_bind(buildbutton,'<ButtonPress-1>',lambda x: Build())

def Extract():
	e.Extract_Resource()
	Scanning()

def SelectSat(x):
	if x == 0 and s.satellite['Sat 0'] > 0:
		s.scanlevel = x
	if x == 1 and s.satellite['Sat 1'] > 0:
		s.scanlevel = x
	if x == 2 and s.satellite['Sat 2'] > 0:
		s.scanlevel = x
	if x == 3 and s.satellite['Sat 3'] > 0:
		s.scanlevel = x
	if x == 4 and s.satellite['Sat 4'] > 0:
		s.scanlevel = x
	if x == 5 and s.satellite['Sat 5'] > 0:
		s.scanlevel = x
	Refresh()

def Scan():
	if (s.scanx[0]+s.ship_x) != None:
		scan.DepleteSat()
		Scanning()
def Scanning():
	if s.satellite['Sat %i'%(s.scanlevel)] == 0:
		Refresh()
	else:
		print "Scanning...."
		Refresh()

		scan_menu = w.create_image(930,350,image=Scan_Menu)

		extractbutton = w.create_text(940,600,text="EXTRACT",font=mainfont)
		w.tag_bind(extractbutton,'<ButtonPress-1>',lambda x: Extract())
		w.create_text(890,100, text='%s %s %s'%(name.QueryName(),name.QueryNumero(),name.QuerySize()),font=mainfont)
		w.create_text(940,280,text='Position:\n x:%i y:%i'%(s.scanx[0]-s.ship_offsetx,-(s.scany[0]-s.ship_offsety)),font=mainfont)
		w.create_text(940,320,text='Planet Type:\n %s'%(scan.Querycolour()),font=mainfont)
		w.create_text(888,415, text='%s'%(scan.Feldspar()),fill='#%02x%02x%02x'%(243,2,8),font=mainfont)
		w.create_text(958,415, text='%s'%(scan.Hydrogen()),fill='#%02x%02x%02x'%(243,2,8),font=mainfont)
		w.create_text(843,495, text="\n".join('%s'%(scan.Uranium())),fill='#%02x%02x%02x'%(243,2,8),font=mainfont)
		w.create_text(1008,495, text="\n".join('%s'%(scan.Oxygen())),fill='#%02x%02x%02x'%(243,2,8),font=mainfont)
		w.create_text(888,555, text='%s'%(scan.Magnetite()),fill='#%02x%02x%02x'%(243,2,8),font=mainfont)
		w.create_text(958,555, text='%s'%(scan.Titanium()),fill='#%02x%02x%02x'%(243,2,8),font=mainfont)
		w.create_polygon(888,425,958,425,998,485,958,545,888,545,848,485,fill='#%02x%02x%02x'%(18,25,59),outline='#%02x%02x%02x'%(66,152,209))
		w.create_polygon(900,446,946,446,973,485,946,525,900,525,874,485,fill='#%02x%02x%02x'%(18,25,59),outline='#%02x%02x%02x'%(66,152,209))
		w.create_polygon(920-scan.Feld()/2,479-scan.Feld()/1.5,927+scan.Hyd()/2,479-scan.Hyd()/1.5,931+scan.Oxy(),485,927+scan.Tita()/2,491+scan.Tita()/1.5,920-scan.Mag()/2,491+scan.Mag()/1.5,916-scan.Uran(),485,fill='#%02x%02x%02x'%(243,2,8),outline='#%02x%02x%02x'%(66,152,209))

def Inventory():
	inventory_screen = Tk()
	inventory_screen.title("Inventory")
	inventory_screen.wm_attributes('-alpha',0.8)
	inventory_screen.update()
	inv = Canvas(inventory_screen,width=400,height=500)
	inv.grid(column=0,row=1)
	Refresh_Inv(inv)
	closebutton = Button(inventory_screen, text="Close", command=inventory_screen.destroy)
	closebutton.grid(column=1, row=0)
	inventory_screen.mainloop()

def Refresh_Inv(inv):
	inv.delete(ALL)
	inv.create_text(50,10,text='Feldspar: %i'%(s.inventory['Feldspar']),fill='#%02x%02x%02x'%(243,2,8),font=mainfont)
	inv.create_text(50,40,text='Hydrogen: %i'%(s.inventory['Hydrogen']),fill='#%02x%02x%02x'%(243,2,8),font=mainfont)
	inv.create_text(50,70,text='Uranium: %i'%(s.inventory['Uranium']),fill='#%02x%02x%02x'%(243,2,8),font=mainfont)
	inv.create_text(50,100,text='Oxygen: %i'%(s.inventory['Oxygen']),fill='#%02x%02x%02x'%(243,2,8),font=mainfont)
	inv.create_text(50,130,text='Magnetite: %i'%(s.inventory['Magnetite']),fill='#%02x%02x%02x'%(243,2,8),font=mainfont)
	inv.create_text(50,160,text='Titanium: %i'%(s.inventory['Titanium']),fill='#%02x%02x%02x'%(243,2,8),font=mainfont)

	inv.create_rectangle(90,5,90+((((float(s.inventory['Feldspar']))/s.storage)*1.6)*100),20,fill='#%02x%02x%02x'%(243,2,8))
	inv.create_rectangle(90,35,90+((((float(s.inventory['Hydrogen']))/s.storage)*1.6)*100),50,fill='#%02x%02x%02x'%(243,2,8))
	inv.create_rectangle(90,65,90+((((float(s.inventory['Uranium']))/s.storage)*1.6)*100),80,fill='#%02x%02x%02x'%(243,2,8))
	inv.create_rectangle(90,95,90+((((float(s.inventory['Oxygen']))/s.storage)*1.6)*100),110,fill='#%02x%02x%02x'%(243,2,8))
	inv.create_rectangle(90,125,90+((((float(s.inventory['Magnetite']))/s.storage)*1.6)*100),140,fill='#%02x%02x%02x'%(243,2,8))
	inv.create_rectangle(90,155,90+((((float(s.inventory['Titanium']))/s.storage)*1.6)*100),170,fill='#%02x%02x%02x'%(243,2,8))

	def Change_Status(x):
		if x == 0:
			inv.itemconfigure(status,text="Feldspar: 20")
		if x == 1:
			inv.itemconfigure(status,text="Feldspar: 50")
		if x == 2:
			inv.itemconfigure(status,text="Feldspar: 80 Magnetite: 30")
		if x == 3:
			inv.itemconfigure(status,text="Feldspar: 100 Magnetite: 50 Titanium: 10")
		if x == 4:
			inv.itemconfigure(status,text="Magnetite: 80 Uranium: 30")
		if x == 5:
			inv.itemconfigure(status,text="Uranium: 50 Titanium: 50")

	sat1 = inv.create_text(60,200,text="Satellite 1 (%s)"%(s.satellite['Sat 0']),activefill='#%02x%02x%02x'%(75,197,25),font=mainfont)
	inv.tag_bind(sat1,'<Enter>',lambda x: Change_Status(0))
	sat2 = inv.create_text(150,200,text="Satellite 2 (%s)"%(s.satellite['Sat 1']),activefill='#%02x%02x%02x'%(75,197,25),font=mainfont)
	inv.tag_bind(sat2,'<Enter>',lambda x: Change_Status(1))
	sat3 = inv.create_text(240,200,text="Satellite 3 (%s)"%(s.satellite['Sat 2']),activefill='#%02x%02x%02x'%(75,197,25),font=mainfont)
	inv.tag_bind(sat3,'<Enter>',lambda x: Change_Status(2))
	sat4 = inv.create_text(60,250,text="Satellite 4 (%s)"%(s.satellite['Sat 3']),activefill='#%02x%02x%02x'%(75,197,25),font=mainfont)
	inv.tag_bind(sat4,'<Enter>',lambda x: Change_Status(3))
	sat5 = inv.create_text(150,250,text="Satellite 5 (%s)"%(s.satellite['Sat 4']),activefill='#%02x%02x%02x'%(75,197,25),font=mainfont)
	inv.tag_bind(sat5,'<Enter>',lambda x: Change_Status(4))
	sat6 = inv.create_text(240,250,text="Satellite 6 (%s)"%(s.satellite['Sat 5']),activefill='#%02x%02x%02x'%(75,197,25),font=mainfont)
	inv.tag_bind(sat6,'<Enter>',lambda x: Change_Status(5))

	inv.tag_bind(sat1,'<ButtonPress-1>',lambda x: BuildGeo1(inv))
	inv.tag_bind(sat2,'<ButtonPress-1>',lambda x: BuildGeo2(inv))
	inv.tag_bind(sat3,'<ButtonPress-1>',lambda x: BuildMed1(inv))
	inv.tag_bind(sat4,'<ButtonPress-1>',lambda x: BuildMed2(inv))
	inv.tag_bind(sat5,'<ButtonPress-1>',lambda x: BuildLow1(inv))
	inv.tag_bind(sat6,'<ButtonPress-1>',lambda x: BuildLow2(inv))

	status = inv.create_text(150,300,text="",fill='#%02x%02x%02x'%(243,2,8),font=mainfont)

def BuildGeo1(inv):
	m.Sat1()
	Refresh_Inv(inv)
def BuildGeo2(inv):
	m.Sat2()
	Refresh_Inv(inv)
def BuildMed1(inv):
	m.Sat3()
	Refresh_Inv(inv)
def BuildMed2(inv):
	m.Sat4()
	Refresh_Inv(inv)
def BuildLow1(inv):
	m.Sat5()
	Refresh_Inv(inv)
def BuildLow2(inv):
	m.Sat6()
	Refresh_Inv(inv)

def Changedisplay():
	scanbutton = w.create_text(900,30,text="SCAN",fill='#%02x%02x%02x'%(243,2,8),font=titlefont)
	w.tag_bind(scanbutton,'<ButtonPress-1>',lambda x: Scan())
	inv_button = w.create_text(300,30,text="INVENTORY",fill='#%02x%02x%02x'%(243,2,8),font=titlefont)
	w.tag_bind(inv_button,'<ButtonPress-1>',lambda x: Inventory())
	ship_coordinates = w.create_text(940,660,text='X: %i, Y: %i'%(s.ship_x,s.ship_y),fill='#%02x%02x%02x'%(243,2,8),font=mainfont)
	minimap = Canvas(w,width=200,height=200, bg='#%02x%02x%02x'%(18,25,59))
	minimap.delete(ALL)
	w.create_window(120,120, window=minimap)

	for n in range(s.planets):
		minimap.create_oval(((s.x[n]-s.ship_offsetx)/100)+100,((s.y[n]-s.ship_offsety)/100)+100,((s.x[n]-s.ship_offsetx)/100)+100,((s.y[n]-s.ship_offsety)/100)+100,outline='white')

	minimap.create_rectangle((s.ship_x/100)+95,(s.ship_y/100)+105,(s.ship_x/100)+105,(s.ship_y/100)+95,outline='green')

	Geo1 = w.create_text(60,610, text='Sat 1: %i'%(s.satellite['Sat 0']),fill='#%02x%02x%02x'%(243,2,8),activefill='#%02x%02x%02x'%(75,197,25),font=mainfont)
	w.tag_bind(Geo1,'<ButtonPress-1>',lambda x: SelectSat(0))
	Geo2 = w.create_text(140,610, text='Sat 2: %i'%(s.satellite['Sat 1']),fill='#%02x%02x%02x'%(243,2,8),activefill='#%02x%02x%02x'%(75,197,25),font=mainfont)
	w.tag_bind(Geo2,'<ButtonPress-1>',lambda x: SelectSat(1))
	Med1 = w.create_text(60,640, text='Sat 3: %i'%(s.satellite['Sat 2']),fill='#%02x%02x%02x'%(243,2,8),activefill='#%02x%02x%02x'%(75,197,25),font=mainfont)
	w.tag_bind(Med1,'<ButtonPress-1>',lambda x: SelectSat(2))
	Med2 = w.create_text(140,640, text='Sat 4: %i'%(s.satellite['Sat 3']),fill='#%02x%02x%02x'%(243,2,8),activefill='#%02x%02x%02x'%(75,197,25),font=mainfont)
	w.tag_bind(Med2,'<ButtonPress-1>',lambda x: SelectSat(3))
	Low1 = w.create_text(60,670, text='Sat 5: %i'%(s.satellite['Sat 4']),fill='#%02x%02x%02x'%(243,2,8),activefill='#%02x%02x%02x'%(75,197,25),font=mainfont)
	w.tag_bind(Low1,'<ButtonPress-1>',lambda x: SelectSat(4))
	Low2 = w.create_text(140,670, text='Sat 6: %i'%(s.satellite['Sat 5']),fill='#%02x%02x%02x'%(243,2,8),activefill='#%02x%02x%02x'%(75,197,25),font=mainfont)
	w.tag_bind(Low2,'<ButtonPress-1>',lambda x: SelectSat(5))

def Refresh():
	w.delete(ALL)
	Changedisplay()
	for n in range(s.planets):
		w.create_oval(s.x[n],s.y[n],s.x[n]+s.planetsize[n],s.y[n]-s.planetsize[n],fill=m.PlanetColour(n),outline='Midnight Blue')


	for n in range(s.planets):
		w.create_image(s.x[n],s.y[n],image = m.Planet(n))


	w.create_image(s.ship_x+s.ship_offsetx,s.ship_y+s.ship_offsety, image = Ship)
	m.Highlight()

	for i in range(len(s.scanx)):
		w.create_oval(s.scanx[i]-10,s.scany[i]+10,s.scanx[i]+30,s.scany[i]-30,outline='#%02x%02x%02x'%(75,197,25),activefill='#%02x%02x%02x'%(243,2,8))

def Left(event):
	s.ship_x = s.ship_x - s.shipspeed
	s.ship_offsetx = s.ship_offsetx + s.shipspeed
	s.x = [a+s.shipspeed for a in s.x]
	Refresh()

def Right(event):
	s.ship_x = s.ship_x + s.shipspeed
	s.ship_offsetx = s.ship_offsetx - s.shipspeed
	s.x = [a-s.shipspeed for a in s.x]
	Refresh()

def Down(event):
	s.ship_y = s.ship_y + s.shipspeed
	s.ship_offsety = s.ship_offsety - s.shipspeed
	s.y = [a-s.shipspeed for a in s.y]
	Refresh()

def Up(event):
	s.ship_y = s.ship_y - s.shipspeed
	s.ship_offsety = s.ship_offsety + s.shipspeed
	s.y = [a+s.shipspeed for a in s.y]
	Refresh()

root.bind('<Left>', Left)
root.bind('<Right>', Right)
root.bind('<Down>', Down)
root.bind('<Up>', Up)

root.mainloop()