import Settings as s
import random as r

firstnames = ['Eros','Gaspra','Ida','Dactyl','Mathilde','Kallispe','Eugenia','Proteus',\
'Alexandria','Laurentia','Jupiter','Arrakis','Lorn','Epitmetheus','Phoebe','Atlas','Prometheus',\
'Charon','Io','Ophelia','Pandora','Iapetus','Ymir','Janus','Paaliaq','Tarvos','Suttungr',\
'Hydra','Nemesis','Phaedra','Kiviuq','Mundiferi','Skathi','Narvi','Aegir','Aegaeon','Oberon',\
'Prospero','Hestia','Xanthippe','Dynamene','Tallon']

numerals = ['I','II','III','IV','V','VI','VII','VIII','XI','X','XI','XII','XIII','XIV','XV',\
'XVI','XVII','']

def QueryName():
	r.seed((s.scanx[0]+s.ship_x))
	rand = r.randrange(0,42)
	return firstnames[rand]

def QueryNumero():
	r.seed((s.scanx[0]+s.ship_x)+1)
	rand = r.randrange(0,18)
	return numerals[rand]

def QuerySize():
	r.seed((s.scany[0]+s.ship_y))
	rand = r.randrange(s.planet_minsize,s.planet_maxsize)
	if rand >= (s.planet_maxsize - 3):
		return 'Prime'
	if rand <= (s.planet_minsize + 3):
		return 'Minor'
	else:
		return ''



