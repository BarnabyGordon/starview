import random as r
import Settings as s

def Querycolour():
	r.seed((s.scanx[0]+s.ship_x))
	rand = r.randrange(0,100)
	if rand >= 0 and rand < 10:
		return 'Oceanic'
	if rand >= 10 and rand < 17:
		return 'Vegetated'
	if rand >= 17 and rand < 40:
		return 'Volcanic'
	if rand >= 40 and rand < 55:
		return 'Gaseous'
	if rand >= 55 and rand < 70:
		return 'Desert'
	if rand >= 70 and rand < 100:
		return 'Rocky'

def Feldspar():
	if s.scanlevel >= 0:
		return 'Feldspar'
	else:
		return '??????'
def Hydrogen():
	if s.scanlevel >= 4:
		return 'Hydrogen'
	else:
		return '??????'
def Uranium():
	if s.scanlevel >= 2:
		return 'Uranium'
	else:
		return '??????'
def Oxygen():
	if s.scanlevel >= 3:
		return 'Oxygen'
	else:
		return '??????'
def Magnetite():
	if s.scanlevel >= 1:
		return 'Magnetite'
	else:
		return '??????'
def Titanium():
	if s.scanlevel >= 5:
		return 'Titanium'
	else:
		return '??????'

def Feld():
	if "%sfeld"%(s.scanx[0]+s.ship_x) not in s.abundance:
		r.seed((s.scanx[0]+s.ship_x))
		xrand = r.randrange(0,100)
		r.seed((s.scany[0]+s.ship_y)+1)
		if xrand >= 0 and xrand < 10:
			feld_abundance = r.randrange(0,31)
		if xrand >= 10 and xrand < 17:
			feld_abundance = r.randrange(0,31)
		if xrand >= 17 and xrand < 40:
			feld_abundance = r.randrange(0,51)
		if xrand >= 40 and xrand < 55:
			feld_abundance = r.randrange(0,1)
		if xrand >= 55 and xrand < 70:
			feld_abundance = r.randrange(0,61)
		if xrand >= 70 and xrand < 100:
			feld_abundance = r.randrange(0,61)
		s.abundance.update({"%sfeld"%(s.scanx[0]+s.ship_x):feld_abundance})
		s.amount.update({"%sfeld"%(s.scanx[0]+s.ship_x):feld_abundance})
		return feld_abundance
	if "%sfeld"%(s.scanx[0]+s.ship_x) in s.abundance:
		return s.amount["%sfeld"%(s.scanx[0]+s.ship_x)]

def Hyd():
	if "%shyd"%(s.scanx[0]+s.ship_x) not in s.abundance:
		r.seed((s.scanx[0]+s.ship_x))
		xrand = r.randrange(0,100)
		r.seed((s.scany[0]+s.ship_y)+2)
		if xrand >= 0 and xrand < 10:
			hyd_abundance = r.randrange(0,31)
		if xrand >= 10 and xrand < 17:
			hyd_abundance = r.randrange(0,31)
		if xrand >= 17 and xrand < 40:
			hyd_abundance = r.randrange(0,41)
		if xrand >= 40 and xrand < 55:
			hyd_abundance = r.randrange(0,61)
		if xrand >= 55 and xrand < 70:
			hyd_abundance = r.randrange(0,1)
		if xrand >= 70 and xrand < 100:
			hyd_abundance = r.randrange(0,1)
		s.abundance.update({"%shyd"%(s.scanx[0]+s.ship_x):hyd_abundance})
		s.amount.update({"%shyd"%(s.scanx[0]+s.ship_x):hyd_abundance})
		return hyd_abundance
	if "%shyd"%(s.scanx[0]+s.ship_x) in s.abundance:
		return s.amount["%shyd"%(s.scanx[0]+s.ship_x)]

def Uran():
	if "%suran"%(s.scanx[0]+s.ship_x) not in s.abundance:
		r.seed((s.scanx[0]+s.ship_x))
		xrand = r.randrange(0,100)
		r.seed((s.scany[0]+s.ship_y)+3)
		if xrand >= 0 and xrand < 10:
			uran_abundance = r.randrange(0,21)
		if xrand >= 10 and xrand < 17:
			uran_abundance = r.randrange(0,31)
		if xrand >= 17 and xrand < 40:
			uran_abundance = r.randrange(0,51)
		if xrand >= 40 and xrand < 55:
			uran_abundance = r.randrange(0,1)
		if xrand >= 55 and xrand < 70:
			uran_abundance = r.randrange(0,41)
		if xrand >= 70 and xrand < 100:
			uran_abundance = r.randrange(0,61)
		s.abundance.update({"%suran"%(s.scanx[0]+s.ship_x):uran_abundance})
		s.amount.update({"%suran"%(s.scanx[0]+s.ship_x):uran_abundance})
		return uran_abundance
	if "%suran"%(s.scanx[0]+s.ship_x) in s.abundance:
		return s.amount["%suran"%(s.scanx[0]+s.ship_x)]

def Oxy():
	if "%soxy"%(s.scanx[0]+s.ship_x) not in s.abundance:
		r.seed((s.scanx[0]+s.ship_x))
		xrand = r.randrange(0,100)
		r.seed((s.scany[0]+s.ship_y)+4)
		if xrand >= 0 and xrand < 10:
			oxy_abundance = r.randrange(0,31)
		if xrand >= 10 and xrand < 17:
			oxy_abundance = r.randrange(0,51)
		if xrand >= 17 and xrand < 40:
			oxy_abundance = r.randrange(0,1)
		if xrand >= 40 and xrand < 55:
			oxy_abundance = r.randrange(0,61)
		if xrand >= 55 and xrand < 70:
			oxy_abundance = r.randrange(0,1)
		if xrand >= 70 and xrand < 100:
			oxy_abundance = r.randrange(0,11)
		s.abundance.update({"%soxy"%(s.scanx[0]+s.ship_x):oxy_abundance})
		s.amount.update({"%soxy"%(s.scanx[0]+s.ship_x):oxy_abundance})
		return oxy_abundance
	if "%soxy"%(s.scanx[0]+s.ship_x) in s.abundance:
		return s.amount["%soxy"%(s.scanx[0]+s.ship_x)]

def Mag():
	if "%smag"%(s.scanx[0]+s.ship_x) not in s.abundance:
		r.seed((s.scanx[0]+s.ship_x))
		xrand = r.randrange(0,100)
		r.seed((s.scany[0]+s.ship_y)+5)
		if xrand >= 0 and xrand < 10:
			mag_abundance = r.randrange(0,31)
		if xrand >= 10 and xrand < 17:
			mag_abundance = r.randrange(0,21)
		if xrand >= 17 and xrand < 40:
			mag_abundance = r.randrange(0,41)
		if xrand >= 40 and xrand < 55:
			mag_abundance = r.randrange(0,1)
		if xrand >= 55 and xrand < 70:
			mag_abundance = r.randrange(0,61)
		if xrand >= 70 and xrand < 100:
			mag_abundance = r.randrange(0,21)
		s.abundance.update({"%smag"%(s.scanx[0]+s.ship_x):mag_abundance})
		s.amount.update({"%smag"%(s.scanx[0]+s.ship_x):mag_abundance})
		return mag_abundance
	if "%smag"%(s.scanx[0]+s.ship_x) in s.abundance:
		return s.amount["%smag"%(s.scanx[0]+s.ship_x)]

def Tita():
	if "%smag"%(s.scanx[0]+s.ship_x) not in s.abundance:
		r.seed((s.scanx[0]+s.ship_x))
		xrand = r.randrange(0,100)
		r.seed((s.scany[0]+s.ship_y)+6)
		if xrand >= 0 and xrand < 10:
			tita_abundance = r.randrange(0,1)
		if xrand >= 10 and xrand < 17:
			tita_abundance = r.randrange(0,21)
		if xrand >= 17 and xrand < 40:
			tita_abundance = r.randrange(0,41)
		if xrand >= 40 and xrand < 55:
			tita_abundance = r.randrange(0,1)
		if xrand >= 55 and xrand < 70:
			tita_abundance = r.randrange(0,61)
		if xrand >= 70 and xrand < 100:
			tita_abundance = r.randrange(0,31)
		s.abundance.update({"%stita"%(s.scanx[0]+s.ship_x):tita_abundance})
		s.amount.update({"%stita"%(s.scanx[0]+s.ship_x):tita_abundance})
		return tita_abundance
	if "%stita"%(s.scanx[0]+s.ship_x) in s.abundance:
		return s.amount["%stita"%(s.scanx[0]+s.ship_x)]

def DepleteSat():
	if (s.satellite['Sat %i'%(s.scanlevel)] - 1) >= 0:
		s.satellite['Sat %i'%(s.scanlevel)] = s.satellite['Sat %i'%(s.scanlevel)] -1






			